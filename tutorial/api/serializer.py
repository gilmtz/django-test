from rest_framework import serializers
from api.models import Contact_info,Place,Event


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact_info
        fields = (
            'phone',
            'website',
            'address_address',
            'address_city',
            'address_state',
            'address_zip'
        )

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = (
            'id',
            'name',
            'description',
            'permalink',
            'pub_date')

class PlaceSerializer(serializers.ModelSerializer):
    contact_info = ContactSerializer(many=True,read_only=True)
    events = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name="events"
    )
    class Meta:
        model = Place
        fields = (
            'id',
            'name',
            'description',
            'contact_info',
            'open_hours',
            'events')



