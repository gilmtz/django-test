import xml.etree.ElementTree as ET 
import requests 
from api.models import Place, Contact_info

def loadRSS(): 
  
    # url of rss feed 
    url = 'https://library.austintexas.gov/events-feed.xml'
  
    # creating HTTP response object from given url 
    resp = requests.get(url) 
    print(type(resp.text))
    line=resp.text.replace(u'\03','')
    print(type(line))
    # print(line)
    # saving the xml file 
    text_file = open('eventsfeed.xml', 'w')
    text_file.write(line)
    text_file.close()
          
  
def parseXML(xmlfile): 
  
    # create element tree object 
    tree = ET.parse(xmlfile) 
  
    # get root element 
    root = tree.getroot() 
  
    # create empty list for news items 
    newsitems = [] 
  
    # iterate news items 
    for item in root.findall('./channel/item'): 
  
        # empty news dictionary 
        news = {} 
  
        # iterate child elements of item 
        for child in item: 
  
            # special checking for namespace object content:media 
            if child.tag == '{http://search.yahoo.com/mrss/}content': 
                news['media'] = child.attrib['url'] 
            else: 
                news[child.tag] = child.text.encode('utf8') 
  
        # append news dictionary to news items list 
        newsitems.append(news) 
      
    # return news items list 
    return newsitems 

def buildEvents():
    # load rss from web to update existing xml file 
    loadRSS() 
    # # parse xml file 
    items = parseXML('eventsfeed.xml') 
    print(items[0])
    for event in items:
        tmp = event["description"].decode("utf-8")
        lib_name_end = tmp.find('-')
        lib = tmp[:lib_name_end].strip()
        description = tmp[lib_name_end+1:].strip()
        name = event['title'].decode("utf-8")
        permalink = event['link'].decode("utf-8")
        pub_date = event['pubDate'].decode('utf-8')
        # print("-----------")
        # print("Name:",name)
        # print("Hosted at:",lib)
        # print("Description:",description)
        # print("Link:",permalink)
        # print("Pub date:",pub_date)
        placeObj = Place.objects.filter(name = name)
        if Place.objects.filter(name = name):
            print(placeObj[0])
            print("There is at least one Entry with the headline Test")


buildEvents()
