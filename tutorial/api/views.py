from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from api.serializer import PlaceSerializer, EventSerializer
from api.models import Place, Event

class PlaceViewSet(viewsets.ModelViewSet):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer

class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
