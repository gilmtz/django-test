import json
from api.models import Place, Contact_info
import requests


def getLibraryList():
    r = requests.get('https://data.austintexas.gov/resource/wzca-8n69.json')
    return r.json()

def createPlaceModel(lib):
    ll = lib.get('latitude_longitude','(0.0,0.0)')
    (latitude,longitude) = parseCordinates(ll)
    
    place = Place(
        name = lib['name'],
        description = lib.get('description',''),
        district = lib.get('district','-1'),
        open_hours = lib.get('open_hours', ''),
        latitude = latitude,
        longitude = longitude
    )
    place.save()
    contact = Contact_info(
        place = place,
        phone = lib.get('phone',''),
        website = lib.get('term_id',''),
        address_address = lib.get('address_address',''),
        address_city = lib.get('address_city',''),
        address_state = lib.get('address_state',''),
        address_zip = lib.get('address_zip',''),
    )
    
    contact.save()
    

def parseCordinates(latitude_longitude):
    try:
        s = eval(string)
        if type(s) == tuple:
            return s
        return (0.0,0.0)
    except:
        return (0.0,0.0)

def parseLibraries():
    json_response = getLibraryList()
    for lib in json_response:
        print(lib['name'])
        createPlaceModel(lib)
