from django.db import models

class Place(models.Model):
    name = models.CharField(max_length = 200,unique=True)
    description = models.CharField(max_length = 200,blank=True,default='')
    district = models.CharField(max_length = 200,blank=True,default='')
    open_hours = models.CharField(max_length = 200,blank=True,default='')
    latitude = models.DecimalField(max_digits=9,decimal_places=6, default = 0.0)
    longitude = models.DecimalField(max_digits=9,decimal_places=6, default = 0.0)

class Contact_info(models.Model):
    place = models.ForeignKey(Place, related_name="contact_info", on_delete=models.CASCADE)
    phone = models.CharField(max_length = 20, blank=True,default='')
    website = models.CharField(max_length = 100, blank=True,default='')
    address_address = models.CharField(max_length = 200, blank=True,default='')
    address_city = models.CharField(max_length = 200, blank=True,default='')
    address_state = models.CharField(max_length = 200, blank=True,default='')
    address_zip = models.CharField(max_length = 200, blank=True,default='')

class Event(models.Model):
    place = models.ForeignKey(Place, related_name="events", on_delete=models.CASCADE)
    name = models.CharField(max_length = 200)
    description = models.CharField(max_length = 200)
    permalink = models.CharField(max_length = 100)
    pub_date = models.DateTimeField('date published')

# class City(models.Model):
#     name = models.CharField(max_length = 100)
#     state = models.CharField(max_length = 100)
#     county = models.CharField(max_length = 100)

